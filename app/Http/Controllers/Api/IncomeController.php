<?php

namespace App\Http\Controllers\Api;

use App\Income;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IncomeController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Income::class, 'income');
    }

    public function index(Request $request)
    {
        return Income::all();
    }

    public function show($income)
    {
        return Income::find($income);
    }

    public function store(Request $request)
    {
        $validator = $this->valid($request);
        $validator['user_id'] = auth()->user()->id;
        $income = Income::create($validator);
        return response()->json([
            'message' => 'Successfully Income Created',
            'income' => $income
        ]);
    }

    public function update(Request $request, Income $income)
    {
        $validator = $this->valid($request);
        $income->update($validator);
        $income->save();
        return response()->json([
            'message' => 'Successfully Income Updated',
            'income' => $income
        ]);
    }

    public function destroy($id)
    {

        Income::destroy($id);
        return response()->json([
            'message' => ' Income Has Been Deleted'
        ]);
    }

    private function valid($request)
    {
        $attribute = $request->validate([
            'name' => 'required',
            'amount' => 'required|numeric|gt:0',
        ]);

        return $attribute;
    }

}
