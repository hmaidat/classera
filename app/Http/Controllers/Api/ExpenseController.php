<?php

namespace App\Http\Controllers\Api;

use App\Expense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpenseController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Expense::class, 'expense');
    }

    public function index(Request $request)
    {
        return Expense::all();
    }

    public function show($expense)
    {
        return Expense::find($expense);
    }

    public function store(Request $request)
    {
        $validator = $this->valid($request);
        $validator['user_id'] = auth()->user()->id;
        $expense = Expense::create($validator);
        return response()->json([
            'message' => 'Successfully Expense Created',
            'expense' => $expense
        ]);
    }

    public function update(Request $request, Expense $expense)
    {
        $validator = $this->valid($request);
        $expense->update($validator);
        $expense->save();

        return response()->json([
            'message' => 'Successfully Expense Updated',
            'expense' => $expense
        ]);
    }

    public function destroy($id)
    {
        Expense::destroy($id);
        return response()->json([
            'message' => 'Expense Has Been Deleted'
        ]);
    }

    private function valid($request)
    {
        $attribute = $request->validate([
            'name' => 'required',
            'amount' => 'required|numeric|gt:0',
        ]);

        return $attribute;
    }

}
