<?php

namespace App\Http\Controllers\Api;

use App\Income;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rules\In;

class BalanceController extends Controller
{

    public function show()
    {
        $user= auth()->user();
        $expenses = $user->expenses;
        $expenseAmount = $this->getExpensesAmount($expenses);
        $incomes = $user->incomes;
        $incomeAmount = $this->getIncomesAmount($incomes);
        $balance = $incomeAmount - $expenseAmount;
        return response()->json([
            'balance' => $balance
        ]);
    }

    public function index()
    {
        $balanceObject = [];
        $users = User::all();
        foreach ($users as $user) {
            $expenses = $user->expenses;
            $expenseAmount = $this->getExpensesAmount($expenses);
            $incomes = $user->incomes;
            $incomeAmount = $this->getIncomesAmount($incomes);
            $balance = $incomeAmount - $expenseAmount;
            $userBalance = (object) [
                'balance' => $balance,
                'user' => $user
            ];
            array_push($balanceObject,$userBalance);
        }
        return response()->json($balanceObject);
    }

    private function getExpensesAmount($expenses)
    {
        $expenseAmount = 0;
        foreach ($expenses as $expense) {
            $expenseAmount += $expense->amount;
        }

        return $expenseAmount;
    }

    private function getIncomesAmount($incomes)
    {
        $incomeAmount = 0;
        foreach ($incomes as $income) {
            $incomeAmount += $income->amount;
        }

        return $incomeAmount;
    }

}


