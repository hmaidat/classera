<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validatedUser = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);

        $validatedUser['password'] = bcrypt($validatedUser['password']);

        $user = User::create($validatedUser);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response(
            ['message' => 'Successfully created', 'user' => $user, 'access_token' => $accessToken] .
            200,
            [
                'Accept' => 'application/json',
                'access-token' => $accessToken,
            ]
        );
    }

    public function login(Request $request)
    {
        $validatedUser = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (auth()->attempt($validatedUser)) {
            $user = auth()->user();
            $accessToken = $user->createToken('authToken')->accessToken;
            return response(
                ['message' => 'Login successfully', 'user' => $user, 'access_token' => $accessToken],
                200,
                [
                    'Accept' => 'application/json',
                    'access-token' => $accessToken,
                ]
            );
        }

        return response(['message' => 'Invalid credentials']);
    }
}
