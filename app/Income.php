<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $fillable= ['name', 'amount', 'user_id'];

    /**
     * Get the user that owns the incomes.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
