<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Income;

class IncomePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(User $user, Income $income)
    {
        return $user->id == $income->user_id;
    }

    public function update(User $user, Income $income)
    {
        return $user->id == $income->user_id;
    }

    public function destroy(User $user, Income $income)
    {
        return $user->id == $income->user_id;
    }

    public function show(User $user, Income $income)
    {
        return $user->id == $income->user_id;
    }

    public function index(User $user, Income $income)
    {
        return $user->id == $income->user_id;
    }


}
