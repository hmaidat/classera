<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Expense;

class ExspensePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function store(User $user, Expense $expense)
    {
        return $user->id == $expense->user_id;
    }

    public function update(User $user, Expense $expense)
    {
        return $user->id == $expense->user_id;
    }

    public function destroy(User $user, Expense $expense)
    {
        return $user->id == $expense->user_id;
    }

    public function show(User $user, Expense $expense)
    {
        return $user->id == $expense->user_id;
    }
    public function index(User $user, Expense $expense)
    {
        return $user->id == $expense->user_id;
    }


}
