<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['name', 'amount', 'user_id'];

    /**
     * Get the user that owns the Expenses.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
